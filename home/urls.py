# Application NAme: home

from django.urls import path
from home.views import (
  home_view, 
  detail_view, 
  count, 
  home_html, 
  installed,
  todo_view
)

urlpatterns = [
  path('home', home_view, name="home"),
  path('', todo_view, name="todo"),
  path('detail/', detail_view, name="detail"),
  path('get_count/count/', count, name="count"),
  path('inst/', installed)
]