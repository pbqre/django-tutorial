from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.conf import settings
from home.models import Todo
# import os

# Create your views here.
TOTAL_VIEWS = 0

def home_view(request):
  # MbYTteYTZT06wbhKw9x0dJe5ghH4VrweSKp9HDpEAphKrIPzYsoauAR6hH2JPp22
  return render(request, "home.html")

def detail_view(request):
  # print(f"\n\n\n\nMethod: {request.method}\n\n\n")
  if request.method == 'POST':
    # print("\n\n\n", request.POST, "\n\n\n")
    print("\n\n\n", f"Name: {request.POST['text']}")
    return HttpResponse('''Hello, I know you're sending POST request.''')
  if request.method == 'GET':
    print("\n\n\n", f"request.GET: {request.GET}")
    return render(request, 'index.html', context={'msg': f'You\'re on a GET request:: request.method -> {request.method}'})

# def count(request):
#   global TOTAL_VIEWS
#   TOTAL_VIEWS += 1
#   return HttpResponse(f"This page is viewed by {TOTAL_VIEWS} times.")

def count(request):
  # with open(file, 'r') as file:
  #   data = json.load(file)
  
  # context {
  #   name:data['name'],


  # }
  # render(request, 'sfs', context=context)
  global TOTAL_VIEWS
  TOTAL_VIEWS += 1
  context = {
    'views': TOTAL_VIEWS,
    'title': 'Page View Counter',
    'data': [
      {
        'name': 'Vishal', 'msg': 'Hello'
      },
      {
        'name': 'Vivek', 'msg': 'Random Msg.'
      },
      {
        'name': 'RAm', 'msg': 'Hello Everyone'
      },
      {
        'name': 'police', 'msg': 'Hello Everyone'
      }
    ]
  }
  return render(request, 'home/count.html', context=context)

def home_html(request):
  return render(request, 'home.html')

def installed(request):
  return HttpResponse(f"{settings.INSTALLED_APPS}")

def todo_view(request):
  context = {
    'todos': Todo.objects.all()
  }
  if request.method == 'POST':
    text = request.POST['todo']
    # todo = Todo()
    # todo.text = text
    # todo.save()
    Todo.objects.create(text=text)
  return render(request, 'home/todo.html', context=context)