# Django Tutorial

## Forms and JSON
- Create a html form
  - name
  - age
  - email
  - class [select]
- POST
  - Save the data of this into a json file.
- Retrieve:
  - using jinja
  - Dynamically render it into a html file :: context.
### Quick Task
- Create A model TODO
  - 1. Create Two Character field.
    - name: CharField.
    - user: CharField
  - 2. Create a url and vie which display this information from the database.
  - 3. To create a form with which you can send the post request and create new todo.


## How to manage a python project using virtual enviroment.
- We'will use `venv` module to create virtual enviroment.
- To create: `python -m venv <name_of_enviroment>`.
- To activate:
  - In Linux: `source ./<name_of_enviroment>/bin/activate`.
  - In Windows: `<name_of_enviroment>\\Scripts\\activate.bat`.
- Deactivate: `deactivate`.
- To install package `pip instal <name_of_package>`.
- To install from requirements.txt: `pip install -r ./requirmetns.txt`.
- Remove the enviroment: Remove the folder created.
- General name of virtual enviroments: `env`, or `venv`.
- To open django shell `python manage.py shell`

## TIPS
- Make sure you know all the basics.

### Some other things that you will have to learn.
- Js
- AJax / Fetch
- Git / Github
- Hosting Django Project.


## Commands:
- https://sqlitebrowser.org/dl/
- http://angel.co
- To make migrations: `python manage.py makemigrations`
- To migrate the changes: `python manage.py migrate`
- To create superuser: `python manage.py createsuperuser`
