from django.db import models

"""
SELECT * FROM TABLE1;
ORM: 
  - Object relational mapping.
  - Without writing SQL query we can perform actions to the database.
"""
class Todo(models.Model):
  # Name of the table: `app_name`_`class_name`
  text = models.CharField(max_length=100)

  def __str__(self):
    return self.text