# Project's Urls.

from django.contrib import admin
from django.urls import path, include
# from django.urls import include


urlpatterns = [
    path('admin_user/', admin.site.urls),
    path('', include("home.urls"))
]
